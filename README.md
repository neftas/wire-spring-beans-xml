Very simple example project that aims to explain the basics of Spring's bean
wiring and dependency injection using XML-based configuration.

It's published as part of my blog post:
https://relentlesscoding.com/2018/09/05/spring-basics-wiring-beans-with-xml-configuration.
