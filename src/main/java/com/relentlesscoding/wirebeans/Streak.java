package com.relentlesscoding.wirebeans;

import java.time.LocalDate;

public abstract class Streak {
    private LocalDate start;
    private LocalDate end;

    public Streak(LocalDate start) {
        this.start = start;
    }

    public LocalDate getStart() {
        return start;
    }

    public LocalDate getEnd() {
        return end;
    }

    public void setEnd(LocalDate end) {
        this.end = end;
    }
}
