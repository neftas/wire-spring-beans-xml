package com.relentlesscoding.wirebeans;

import java.time.LocalDate;

public class NegativeStreak extends Streak {
    public NegativeStreak(LocalDate start) {
        super(start);
    }
}
