package com.relentlesscoding.wirebeans;

import java.time.LocalDate;

public class PositiveStreak extends Streak {
    public PositiveStreak(LocalDate start) {
        super(start);
    }
}
